`docker container ls`

Individuare l'`<id>` del container con MC installato

`docker container commit <id>`

`docker image ls`

Individuare l'`<id>`dell'immagine appena creata

`docker image tag <id> ubuntu-mc`

`docker container run -it ubuntu-mc`

`mc`