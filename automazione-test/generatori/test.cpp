#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include "aritmetica.h"

TEST_CASE("somma numeri positivi") {
  int x = GENERATE(3, 5, 6);
  int y = GENERATE(2, 3, 5);
  REQUIRE(add(x, y) == x + y);
}

TEST_CASE("somma numeri positivi, nulli e negativi"){
    int x = GENERATE(range(-100, 100));
    int y = GENERATE(range(-100, 100));
    REQUIRE(add(x,y) == x + y);
}