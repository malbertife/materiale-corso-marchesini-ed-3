int fatt(int n){
    int p = 1;
    while (n > 1){
        p *= n;
        n--;
    }
    return p;
}