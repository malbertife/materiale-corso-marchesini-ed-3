#include <rapidcheck.h>

#include "aritmetica.h"

int main() {
  rc::check("Elemento neutro", [](int x) { RC_ASSERT(add(x, 0) == x); });

  rc::check("Associativa", [](int x, int y, int z) {
    RC_ASSERT(add(add(x, y), z) == add(x, add(y, z)));
  });

  rc::check("Commutativa", [](int x, int y){
    RC_ASSERT(add(x, y) == add(y, x));
  });
  
  return 0;
}
